cmake_minimum_required(VERSION 3.19)
project(untitled2)

set(CMAKE_CXX_STANDARD 14)
SET(CMAKE_CXX_FLAGS -pthread)

add_executable(graphLabeler main.cpp graph.cpp graph.h reader/reader.cpp reader/reader.h)
