//
// Created by matthew on 12.11.21.
//



#include "reader.h"

#include <utility>

void reader::read_header() {
    std::ifstream myFile(path, std::ios::in);
    std::string size;

    if (myFile.is_open()) { // always check whether the file is open
        myFile >> size; // pipe file's content into stream
        myFile.close();
        this->numNodes = std::stoi(size);
        this->exists = true;
    }
}

void reader::read(uint8_t *a) const {
    std::ifstream myFile(path);
    std::string read;

    if (myFile.is_open()) { // always check whether the file is open
        myFile >> read; // pipe file's content into stream
    }

    for (int i = 0; i < numNodes; ++i) {
        for (int j = 0; j < numNodes; ++j) {
            std::string nextString;
            myFile >> nextString;

            uint8_t next = std::stoi(nextString);
            *(a + i * numNodes + j) = next;
        }
    }

    myFile.close();
}

reader::reader(std::string path) : path(std::move(path)), numNodes(0) , exists(false){
    this->read_header();
}
