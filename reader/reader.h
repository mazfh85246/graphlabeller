//
// Created by matthew on 12.11.21.
//

#ifndef UNTITLED2_READER_H
#define UNTITLED2_READER_H


#include <cstddef>
#include <string>
#include <iostream>
#include <fstream>
#include <string>

class reader {
public:
    std::string path;

    size_t numNodes;

    bool exists;

    void read_header();

    void read(uint8_t * a) const;
    reader(std::string path);
};


#endif //UNTITLED2_READER_H
