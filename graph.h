//
// Created by matthew on 02.10.21.
//

#ifndef UNTITLED2_GRAPH_H
#define UNTITLED2_GRAPH_H


class Graph {
public:
    uint8_t *edges;
    uint8_t numNodes;
public:
    Graph(uint8_t *a, uint32_t numNode);

    bool is_edge(uint8_t x, uint8_t y);

    uint8_t getNumNodes();
};

Graph getCircleGraph(uint32_t numNodes, uint8_t * a);

Graph getAlmostCompleteGraph(uint32_t numNodes, uint8_t * a);


#endif //UNTITLED2_GRAPH_H
