# Graph Labeller for (Mod-)Minus Graphs

The main purpose of this project is to label the given graphs, using a Minus labelling.

If the graph cannot be labelled, or the labels required are too large, then extra nodes are added until the graph can be
labelled with labels of linear size.

## Usage

### Input

The input is in the graphs directory. In order to add a new graph to be labelled, add a new file. The name of the file 
must be a number. Do not leave any gaps between the numbers of file (i.e. do not have the files 1, 2, 4, but 1, 2, 3, 4).
The first line of the file is the number of nodes of the graph. After that is the adjacency matrix of the graph given in
whitespace seperated values format.

### Output

Labellings of the given graphs (a limited amount if there are a lot) are outputted in files of the same name in the 
results directory.

### Running the program

1. Clone this repository
2. Place your graphs in the graphs directory
3. Open the executable directory in your terminal
3. Set your environment variables if you want anything but the defaults
3. run the executable

## Environment variables

### NUM_THREADS
This defines the number of threads to use. If your graphs are small, it is recommended to use a small number of threads,
otherwise the output might not be understandable (this is done for efficiency reasons).
Default is 8

### NUM_RESULTS
The maximum number of labellings to output for the graph.
Default is 40 (for no specific reason)

### LABEL_LIMIT
The absolute value of the largest label that one should try to use to label the graph.
Per default, linear sized max labels will be used (20 * (numNodes + numExtra))

### NUM_EXTRA
The Number of extra vertices one can try to add to the graph, to turn a non-(M)MG to a (M)MG.
Default is 0.

### MIN_MODULO
When this is used, the graph is tested for MG-ness.
Otherwise, this defines the minimum modulo to test.
In the case that this is set, LABEL_LIMIT decides the maximum modulo to try
