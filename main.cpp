#include <iostream>
#include "graph.h"
#include <cmath>
#include <thread>
#include <vector>
#include "cstring"
#include "reader/reader.h"

void printArray(const int16_t *array, uint64_t length);

bool checkGuess(Graph g, int16_t *guess, uint8_t numExtra, uint8_t limit, const uint8_t *lookupTable);

bool guess(Graph g, uint8_t limit, uint8_t numExtra);

bool resetLookupTable(uint8_t *table, int16_t tableSize, const int16_t *inputs, uint8_t numInputs);

bool getFromLookupTable(const uint8_t *table, uint8_t half_limit, int16_t i, uint8_t limit);

void increment(int16_t *guess, uint8_t size, uint8_t limit);

bool moduloGuess(Graph graph, int16_t limit, int16_t i);

bool checkGuessModulo(Graph graph, int16_t *guess, uint8_t extra, uint8_t modulo, uint8_t *lookupTable);

static std::ofstream currentWriteFile = std::ofstream();

int NUM_THREADS = 8;
int NUM_RESULTS = 40;
int LABEL_LIMIT = -1;
int NUM_EXTRA = 0;
int MIN_MODULO = -1;
int DISTINCT = 0;

int main() {
    auto numThreads = std::getenv("NUM_THREADS");
    auto numResults = std::getenv("NUM_RESULTS");
    auto limit = std::getenv("LABEL_LIMIT");
    auto maxExtra = std::getenv("MAX_EXTRA_VERTICES");
    auto modulo = std::getenv("MIN_MODULO");
    auto distinct = std::getenv("DISTINCT");


    if (numThreads != nullptr) {
        NUM_THREADS = std::stoi(numThreads);
    }

    if (numResults != nullptr) {
        NUM_RESULTS = std::stoi(numResults);
    }

    if (limit != nullptr) {
        LABEL_LIMIT = std::stoi(limit);
    }

    if (maxExtra != nullptr) {
        NUM_EXTRA = std::stoi(maxExtra);
    }

    if (distinct != nullptr) {
        DISTINCT = std::stoi(distinct);
    }

    if (modulo != nullptr) {
        if (LABEL_LIMIT == -1) {
            std::cout << "Please define a label limit when using modulo \n";
        }
        MIN_MODULO = std::stoi(modulo);
    }

    std::cout << "number of threads: " << NUM_THREADS << "\n";
    std::cout << "number of results: " << NUM_RESULTS << "\n";
    std::cout << "label limit: " << LABEL_LIMIT << "\n";
    std::cout << "Starting Modulo: " << MIN_MODULO << "\n";
    for (int j = 0; j < INTMAX_MAX; ++j) {
        reader r("../graphs/" + std::to_string(j));

        std::cout << "Starting work on graph number " << j << '\n';

        if (!r.exists) {
            break;
        }

        currentWriteFile.open("../results/" + std::to_string(j), std::ofstream::out);

        uint32_t numNodes = r.numNodes;

        uint8_t a[numNodes][numNodes];
        r.read(a[0]);
        Graph g = {a[0], numNodes};

        int16_t i = 0;
        int16_t labelLimit = (LABEL_LIMIT == -1) ? 20 * (numNodes) : LABEL_LIMIT;

        while (i <= NUM_EXTRA && ((MIN_MODULO == -1 && !guess(g, labelLimit, i)) ||
                                  (MIN_MODULO != -1 && !moduloGuess(g, labelLimit, i)))) {
            std::cout << "No labelling found with " << i << " extra vertices" << '\n';
            i++;
            labelLimit = (LABEL_LIMIT == -1) ? 20 * (numNodes + i) : LABEL_LIMIT;
        }
        currentWriteFile.close();
    }

    return 0;
}

void guessModuloThreadMethod(Graph g, uint8_t modulo, uint8_t numExtra, int threadNumber, int *numFound) {
    uint8_t lookupTable[modulo + 1];
    const uint8_t num_nodes = g.getNumNodes();
    int16_t iterator[num_nodes + numExtra + 1];
    memset(iterator, 0, (num_nodes + numExtra + 1) * sizeof(int16_t));
    for (int i = 0; i < threadNumber; ++i) {
        increment(iterator, num_nodes + numExtra + 1, modulo);
    }

    uint32_t last = num_nodes + numExtra;

    while (iterator[last] == 0) {
        bool distinct = resetLookupTable(lookupTable, modulo + 1, iterator, num_nodes + numExtra);
        auto found = !(DISTINCT && !distinct) && checkGuessModulo(g, iterator, numExtra, modulo, lookupTable);
        for (int i = 0; i < NUM_THREADS; ++i) {
            increment(iterator, num_nodes + numExtra + 1, modulo);
        }

        if (*numFound > NUM_RESULTS) {
            return;
        }

        if (found) {
            *numFound = *(numFound) + 1;
        }
    }
}

bool checkGuessModulo(Graph g, int16_t *guess, uint8_t extra, uint8_t modulo, uint8_t *lookupTable) {
    const uint32_t num_nodes = g.getNumNodes();

    for (uint32_t x = 0; x < num_nodes; ++x) {
        for (int y = 0; y < num_nodes; ++y) {
            if (x == y) {
                continue;
            }

            int16_t i = (guess[x] - guess[y]) % modulo;
            i = (i < 0) ? i + modulo : i;
            bool shouldBeEdge = getFromLookupTable(lookupTable, 0, i, modulo);
            if (g.is_edge(x, y)) {
                if (!shouldBeEdge) {
                    return false;
                }
            } else if (shouldBeEdge) {
                return false;
            }
        }
    }

    printArray(guess, num_nodes + extra);
    return true;
}


bool moduloGuess(Graph graph, int16_t limit, int16_t extra) {
    std::vector<std::thread> threads[NUM_THREADS];
    int numLabels = 0;
    for (int m = MIN_MODULO; m < limit; ++m) {
        std::cout << "Starting Work on Modulo: " << m << '\n';
        for (int i = 0; i < NUM_THREADS; ++i) {
            threads->push_back(std::thread(guessModuloThreadMethod, graph, m, extra, i, &numLabels));
        }

        for (int i = 0; i < NUM_THREADS; ++i) {
            threads->back().join();
            threads->pop_back();
        }

        if (numLabels > 0) {
            return true;
        }
    }
    return false;
}

bool resetLookupTable(uint8_t *table, int16_t tableSize, const int16_t *inputs, uint8_t numInputs) {
    memset(table, 0, tableSize);
    bool distinct = true;
    for (int i = 0; i < numInputs; ++i) {
        distinct &= table[inputs[i]] != 1;
        table[inputs[i]] = 1;
    }
    return distinct;
}

void guessThreadMethod(Graph g, uint8_t limit, uint8_t numExtra, int threadNumber, int *numFound) {
    uint8_t lookupTable[limit + 1];
    const uint8_t num_nodes = g.getNumNodes();
    int16_t iterator[num_nodes + numExtra + 1];
    memset(iterator, 0, (num_nodes + numExtra + 1) * sizeof(int16_t));
    for (int i = 0; i < threadNumber; ++i) {
        increment(iterator, num_nodes + numExtra + 1, limit);
    }

    uint32_t last = num_nodes + numExtra;
    while (iterator[last] == 0) {
        uint16_t minus = limit / 2;
        int16_t guess[num_nodes + numExtra];

        bool distinct = resetLookupTable(lookupTable, limit + 1, iterator, num_nodes + numExtra);
        bool found = false;
        if (!DISTINCT || distinct) {
            for (int j = 0; j < num_nodes + numExtra; ++j) {
                guess[j] = iterator[j] - minus;
            }

            found = checkGuess(g, guess, numExtra, limit, lookupTable);
        }
        for (int i = 0; i < NUM_THREADS; ++i) {
            increment(iterator, num_nodes + numExtra + 1, limit);
        }

        if (*numFound > NUM_RESULTS) {
            return;
        }

        if (found) {
            *numFound = *(numFound) + 1;
        }
    }
}

bool guess(Graph g, uint8_t limit, uint8_t numExtra) {
    std::vector<std::thread> threads[NUM_THREADS];
    int numLabels = 0;
    for (int i = 0; i < NUM_THREADS; ++i) {
        threads->push_back(std::thread(guessThreadMethod, g, limit, numExtra, i, &numLabels));
    }

    for (int i = 0; i < NUM_THREADS; ++i) {
        threads->back().join();
        threads->pop_back();
    }

    return numLabels > 0;
}


bool checkGuess(Graph g, int16_t *guess, uint8_t numExtra, uint8_t limit, const uint8_t *lookupTable) {
    const uint32_t num_nodes = g.getNumNodes();
    uint8_t halfLimit = limit / 2;

    for (uint32_t x = 0; x < num_nodes; ++x) {
        for (int y = 0; y < num_nodes; ++y) {
            if (x == y) {
                continue;
            }

            bool shouldBeEdge = getFromLookupTable(lookupTable, halfLimit, (guess[x] - guess[y]), limit);
            if (g.is_edge(x, y)) {
                if (!shouldBeEdge) {
                    return false;
                }
            } else if (shouldBeEdge) {
                return false;
            }
        }
    }

    printArray(guess, num_nodes + numExtra);
    return true;
}

bool getFromLookupTable(const uint8_t *table, uint8_t half_limit, int16_t i, uint8_t limit) {
    uint64_t a = i + half_limit;
    return (a <= limit) && table[a] == 1;
}

void increment(int16_t *guess, uint8_t size, uint8_t limit) {
    for (uint32_t i = 0; i < size; ++i) {
        if (guess[i] < limit) {
            guess[i]++;
            return;
        }
        guess[i] = 0;
    }
}

void printArray(const int16_t *array, uint64_t length) {
    currentWriteFile << '[';
    for (int i = 0; i < length; i++) {
        currentWriteFile << *(array + i) << ',';
    }

    currentWriteFile << ']' << '\n';
}

