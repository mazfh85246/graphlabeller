//
// Created by matthew on 02.10.21.
//

#include <malloc.h>
#include <cstdint>
#include "graph.h"



bool Graph::is_edge(uint8_t x, uint8_t y) {
    return edges[x*numNodes + y] == 1;
}

Graph::Graph(uint8_t *a, uint32_t numNode) {
    edges = a;
    numNodes = numNode;
}

uint8_t Graph::getNumNodes() {
    return numNodes;
}

Graph getCircleGraph(const uint32_t numNodes, uint8_t * a) {
    for (int i = 0; i < numNodes; ++i) {
        for (int64_t j = 0; j < numNodes; ++j) {
            int64_t abs = j - 1 % numNodes;
            abs = (abs < 0) ? abs + numNodes : abs;
            a[i * numNodes + j] = (i != abs % numNodes) ? 0 : 1;
        }
    }

    return {a, numNodes};
}

Graph getAlmostCompleteGraph(const uint32_t numNodes, uint8_t * a){
    for (int i = 0; i < numNodes; ++i) {
        for (int j = 0; j < numNodes; ++j) {
            a[i * numNodes + j] = (i == j || i == numNodes - 1 || j == numNodes - 1)? 0 : 1;
        }
    }
    return {a,numNodes};
}